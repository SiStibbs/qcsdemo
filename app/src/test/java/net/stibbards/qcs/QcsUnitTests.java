package net.stibbards.qcs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.stibbards.qcs.data.Commit;
import net.stibbards.qcs.utils.CommitDeserializer;
import net.stibbards.qcs.webservice.GitHubService;
import net.stibbards.qcs.webservice.RetrofitClientInstance;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class QcsUnitTests {

    @Test
    public void testJsonParser() {
        CommitDeserializer deserializer = new CommitDeserializer();

        String jsonString = getCommitJsonString();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Commit.class, deserializer);
        Gson gson = gsonBuilder.create();

        Commit c = gson.fromJson(jsonString, Commit.class);

        assertEquals("12a1b0296b3a16265af34dfe099c45d31cd5601e", c.getSha());
        assertEquals(c.getAuthorName(), "Kirill Shmakov");
        assertEquals(c.getAuthorImage(), "https://avatars3.githubusercontent.com/u/1360111?v=4");
        assertEquals(c.getDate(), "2019-07-24T12:46:11Z");
        assertEquals(c.getTitle(), "Clean up useless code");
    }

    @Test
    public void testDownloader() throws IOException {
        GitHubService service = RetrofitClientInstance.getRetrofitInstance(Commit.class, new CommitDeserializer()).create(GitHubService.class);
        Call<List<Commit>> call = service.getAllCommits();
        Response<List<Commit>> response = call.execute();
        assertNotNull(response);
        List<Commit> commits = response.body();
        assertNotNull(commits);

        assertNotEquals(commits.size(), 0);
    }

    private String getCommitJsonString() {
        return "{\n" +
                "        \"sha\": \"12a1b0296b3a16265af34dfe099c45d31cd5601e\",\n" +
                "        \"node_id\": \"MDY6Q29tbWl0MzQzMjI2NjoxMmExYjAyOTZiM2ExNjI2NWFmMzRkZmUwOTljNDVkMzFjZDU2MDFl\",\n" +
                "        \"commit\": {\n" +
                "            \"author\": {\n" +
                "                \"name\": \"Kirill Shmakov\",\n" +
                "                \"email\": \"kirill.shmakov@jetbrains.com\",\n" +
                "                \"date\": \"2019-07-24T12:46:11Z\"\n" +
                "            },\n" +
                "            \"committer\": {\n" +
                "                \"name\": \"Kirill Shmakov\",\n" +
                "                \"email\": \"kirill.shmakov@jetbrains.com\",\n" +
                "                \"date\": \"2019-07-24T15:25:11Z\"\n" +
                "            },\n" +
                "            \"message\": \"Clean up useless code\",\n" +
                "            \"tree\": {\n" +
                "                \"sha\": \"81d911e4fd731788348cc11cab830806ce99b2f5\",\n" +
                "                \"url\": \"https://api.github.com/repos/JetBrains/kotlin/git/trees/81d911e4fd731788348cc11cab830806ce99b2f5\"\n" +
                "            },\n" +
                "            \"url\": \"https://api.github.com/repos/JetBrains/kotlin/git/commits/12a1b0296b3a16265af34dfe099c45d31cd5601e\",\n" +
                "            \"comment_count\": 0,\n" +
                "            \"verification\": {\n" +
                "                \"verified\": false,\n" +
                "                \"reason\": \"unsigned\",\n" +
                "                \"signature\": null,\n" +
                "                \"payload\": null\n" +
                "            }\n" +
                "        },\n" +
                "        \"url\": \"https://api.github.com/repos/JetBrains/kotlin/commits/12a1b0296b3a16265af34dfe099c45d31cd5601e\",\n" +
                "        \"html_url\": \"https://github.com/JetBrains/kotlin/commit/12a1b0296b3a16265af34dfe099c45d31cd5601e\",\n" +
                "        \"comments_url\": \"https://api.github.com/repos/JetBrains/kotlin/commits/12a1b0296b3a16265af34dfe099c45d31cd5601e/comments\",\n" +
                "        \"author\": {\n" +
                "            \"login\": \"kishmakov\",\n" +
                "            \"id\": 1360111,\n" +
                "            \"node_id\": \"MDQ6VXNlcjEzNjAxMTE=\",\n" +
                "            \"avatar_url\": \"https://avatars3.githubusercontent.com/u/1360111?v=4\",\n" +
                "            \"gravatar_id\": \"\",\n" +
                "            \"url\": \"https://api.github.com/users/kishmakov\",\n" +
                "            \"html_url\": \"https://github.com/kishmakov\",\n" +
                "            \"followers_url\": \"https://api.github.com/users/kishmakov/followers\",\n" +
                "            \"following_url\": \"https://api.github.com/users/kishmakov/following{/other_user}\",\n" +
                "            \"gists_url\": \"https://api.github.com/users/kishmakov/gists{/gist_id}\",\n" +
                "            \"starred_url\": \"https://api.github.com/users/kishmakov/starred{/owner}{/repo}\",\n" +
                "            \"subscriptions_url\": \"https://api.github.com/users/kishmakov/subscriptions\",\n" +
                "            \"organizations_url\": \"https://api.github.com/users/kishmakov/orgs\",\n" +
                "            \"repos_url\": \"https://api.github.com/users/kishmakov/repos\",\n" +
                "            \"events_url\": \"https://api.github.com/users/kishmakov/events{/privacy}\",\n" +
                "            \"received_events_url\": \"https://api.github.com/users/kishmakov/received_events\",\n" +
                "            \"type\": \"User\",\n" +
                "            \"site_admin\": false\n" +
                "        },\n" +
                "        \"committer\": {\n" +
                "            \"login\": \"kishmakov\",\n" +
                "            \"id\": 1360111,\n" +
                "            \"node_id\": \"MDQ6VXNlcjEzNjAxMTE=\",\n" +
                "            \"avatar_url\": \"https://avatars3.githubusercontent.com/u/1360111?v=4\",\n" +
                "            \"gravatar_id\": \"\",\n" +
                "            \"url\": \"https://api.github.com/users/kishmakov\",\n" +
                "            \"html_url\": \"https://github.com/kishmakov\",\n" +
                "            \"followers_url\": \"https://api.github.com/users/kishmakov/followers\",\n" +
                "            \"following_url\": \"https://api.github.com/users/kishmakov/following{/other_user}\",\n" +
                "            \"gists_url\": \"https://api.github.com/users/kishmakov/gists{/gist_id}\",\n" +
                "            \"starred_url\": \"https://api.github.com/users/kishmakov/starred{/owner}{/repo}\",\n" +
                "            \"subscriptions_url\": \"https://api.github.com/users/kishmakov/subscriptions\",\n" +
                "            \"organizations_url\": \"https://api.github.com/users/kishmakov/orgs\",\n" +
                "            \"repos_url\": \"https://api.github.com/users/kishmakov/repos\",\n" +
                "            \"events_url\": \"https://api.github.com/users/kishmakov/events{/privacy}\",\n" +
                "            \"received_events_url\": \"https://api.github.com/users/kishmakov/received_events\",\n" +
                "            \"type\": \"User\",\n" +
                "            \"site_admin\": false\n" +
                "        },\n" +
                "        \"parents\": [\n" +
                "            {\n" +
                "                \"sha\": \"d3bc2ec855abd3ace67821f2f0ae071f5eaba106\",\n" +
                "                \"url\": \"https://api.github.com/repos/JetBrains/kotlin/commits/d3bc2ec855abd3ace67821f2f0ae071f5eaba106\",\n" +
                "                \"html_url\": \"https://github.com/JetBrains/kotlin/commit/d3bc2ec855abd3ace67821f2f0ae071f5eaba106\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }";
    }
}