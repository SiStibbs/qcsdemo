package net.stibbards.qcs;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import net.stibbards.qcs.data.AppDatabase;
import net.stibbards.qcs.data.Commit;
import net.stibbards.qcs.data.CommitDao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class EntityReadWriteTest {

    private CommitDao commitDao;
    private AppDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        commitDao = db.commitDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void writeUserAndReadInList() throws Exception {
        Commit commit = new Commit();
        commit.setSha("0123456");
        commit.setTitle("Test Title");
        commitDao.insert(commit);

        Commit savedCommit = commitDao.getBySha("0123456");
        assertNotNull(savedCommit);
        assertEquals(savedCommit, commit);
        assertEquals(savedCommit.getTitle(), commit.getTitle());
    }
}
