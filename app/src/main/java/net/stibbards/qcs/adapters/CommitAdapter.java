package net.stibbards.qcs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import net.stibbards.qcs.BR;
import net.stibbards.qcs.data.Commit;
import net.stibbards.qcs.R;
import net.stibbards.qcs.utils.CommitDiffCallback;

import java.util.List;

public class CommitAdapter extends RecyclerView.Adapter<CommitAdapter.ViewHolder> {

    private List<Commit> items;

    public CommitAdapter() {

    }

    public void setItems(List<Commit> items) {
        if(this.items != null) {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new CommitDiffCallback(this.items, items));
            diffResult.dispatchUpdatesTo(this);
            this.items.clear();
            this.items.addAll(items);
        }
        else {
            this.items = items;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.bind(getItem(position));
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.commit_row, parent, false);
        return new ViewHolder(binding);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ViewDataBinding binding;

        ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Object obj) {
            binding.setVariable(BR.commit, obj);
            binding.executePendingBindings();
        }
    }

    private Commit getItem(int position) {
        if(items != null && position < items.size()) {
            return items.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if(items != null) {
            return items.size();
        }
        return 0;
    }
}
