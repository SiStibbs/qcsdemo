package net.stibbards.qcs;

import android.app.Application;

import androidx.room.Room;

import net.stibbards.qcs.data.AppDatabase;
import net.stibbards.qcs.data.Commit;
import net.stibbards.qcs.data.CommitRepository;
import net.stibbards.qcs.utils.CommitDeserializer;
import net.stibbards.qcs.utils.ThreadExecutor;
import net.stibbards.qcs.webservice.GitHubService;
import net.stibbards.qcs.webservice.RetrofitClientInstance;

import java.util.concurrent.Executor;

public class App extends Application {

    public static AppDatabase database;
    public static Executor executor;
    private static GitHubService service;

    @Override
    public void onCreate() {
        super.onCreate();

        executor = new ThreadExecutor();
        database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "QCS").build();
        service = RetrofitClientInstance.getRetrofitInstance(Commit.class, new CommitDeserializer()).create(GitHubService.class);
    }

    public static CommitRepository getRepository() {
        return CommitRepository.getInstance(service, database.commitDao(), executor);
    }
}
