package net.stibbards.qcs.webservice;

import net.stibbards.qcs.data.Commit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GitHubService {

    @GET("repos/JetBrains/kotlin/commits")
    Call<List<Commit>> getAllCommits();
}
