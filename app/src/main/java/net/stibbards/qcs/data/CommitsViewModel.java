package net.stibbards.qcs.data;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import net.stibbards.qcs.App;

import java.util.List;

public class CommitsViewModel extends AndroidViewModel {

    private CommitRepository repository;

    private final MediatorLiveData<List<Commit>> observableCommits;

    public CommitsViewModel(Application application) {
        super(application);

        observableCommits = new MediatorLiveData<>();
        observableCommits.setValue(null);

        repository = App.getRepository();
        LiveData<List<Commit>> commits = repository.getCommits();

        // observe the changes of the products from the database and forward them
        observableCommits.addSource(commits, observableCommits::setValue);
    }

    public LiveData<List<Commit>> getCommits() {
        return observableCommits;
    }

    public void refreshData() {
        repository.refreshData();
    }

    public LiveData<List<Commit>> searchQuery(String query) {
        return repository.searchFor(query);
    }
}
