package net.stibbards.qcs.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Commit {

    @PrimaryKey
    @NonNull
    private String sha;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "authorImage")
    private String authorImage;

    @ColumnInfo(name = "authorName")
    private String authorName;

    @ColumnInfo(name = "date")
    private String date;
    public Commit() {

    }

    @Ignore
    public Commit(@NonNull String sha, String title, String authorImage, String authorName, String date) {
        this.sha = sha;
        this.title = title;
        this.authorImage = authorImage;
        this.authorName = authorName;
        this.date = date;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorImage() {
        return authorImage;
    }

    public void setAuthorImage(String authorImage) {
        this.authorImage = authorImage;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commit commit = (Commit) o;
        return sha.equals(commit.sha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sha);
    }
}
