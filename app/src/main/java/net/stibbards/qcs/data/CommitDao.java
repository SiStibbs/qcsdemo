package net.stibbards.qcs.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CommitDao {

    @Query("SELECT * FROM `Commit`")
    LiveData<List<Commit>> getAll();

    @Query("SELECT * FROM `Commit` WHERE sha = :sha LIMIT 1")
    Commit getBySha(String sha);

    @Query("SELECT * FROM 'Commit' WHERE title LIKE :searchquery")
    LiveData<List<Commit>> searchFor(String searchquery);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Commit> commits);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Commit commit);

    @Delete
    void delete(Commit commit);
}
