package net.stibbards.qcs.data;

import android.util.Log;

import androidx.lifecycle.LiveData;

import net.stibbards.qcs.webservice.GitHubService;

import java.util.List;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class CommitRepository {

    private static final String TAG = "CommitRepository";

    private GitHubService webservice;
    private CommitDao commitDao;
    private Executor executor;

    private static CommitRepository instance;

    public static CommitRepository getInstance(GitHubService webservice, CommitDao commitDao, Executor executor) {
        if(instance == null) {
            instance = new CommitRepository(webservice, commitDao, executor);
        }
        return instance;
    }

    private CommitRepository(GitHubService webservice, CommitDao commitDao, Executor executor) {
        this.webservice = webservice;
        this.commitDao = commitDao;
        this.executor = executor;
    }

    public LiveData<List<Commit>> getCommits() {
        refreshData();
        return commitDao.getAll();
    }

    public LiveData<List<Commit>> searchFor(String query) {
        if(query != null) {
            return commitDao.searchFor(query);
        }
        else {
            return commitDao.getAll();
        }
    }

    public void refreshData() {
        executor.execute(() -> {
            Call<List<Commit>> call = webservice.getAllCommits();
            call.enqueue(new Callback<List<Commit>>() {
                @Override
                @EverythingIsNonNull
                public void onResponse(Call<List<Commit>> call, Response<List<Commit>> response) {
                    executor.execute(() -> {
                        commitDao.insertAll(response.body());
                    });
                }

                @Override
                @EverythingIsNonNull
                public void onFailure(Call<List<Commit>> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t);
                }
            });
        });

    }
}
