package net.stibbards.qcs.utils;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import net.stibbards.qcs.data.Commit;

import java.lang.reflect.Type;

public class CommitDeserializer implements JsonDeserializer<Commit> {

    private static final String TAG = "CommitDeserializer";

    @Override
    public Commit deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        final JsonObject item = json.getAsJsonObject();

        try {
            if (!item.isJsonNull()) {
                final String sha = item.get("sha").getAsString();

                String message = null;
                String authorName = null;
                String date = null;
                if (!item.get("commit").isJsonNull()) {
                    JsonObject commit = item.get("commit").getAsJsonObject();
                    message = commit.get("message").getAsString();

                    if (commit.get("author") != null) {
                        JsonObject author = commit.get("author").getAsJsonObject();
                        authorName = author.get("name").getAsString();
                        date = author.get("date").getAsString();
                    }
                }


                String authorImage = null;
                if (!item.get("author").isJsonNull()) {
                    JsonObject authorDetails = item.get("author").getAsJsonObject();
                    authorImage = authorDetails.get("avatar_url").getAsString();
                }

                return new Commit(sha, message, authorImage, authorName, date);
            }
        } catch (Exception e) {
            Log.e(TAG, "deserialize: ", e);
        }
        return null;
    }
}
