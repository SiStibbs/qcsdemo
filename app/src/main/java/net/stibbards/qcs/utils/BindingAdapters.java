package net.stibbards.qcs.utils;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.squareup.picasso.Picasso;

public class BindingAdapters {

    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter(value={"imageUrl", "placeholder"}, requireAll=false)
    public static void setImageUrl(ImageView imageView, String url, Drawable placeHolder) {
        if (TextUtils.isEmpty(url)) {
            if(placeHolder != null) {
                imageView.setImageDrawable(placeHolder);
            }
        } else {
            Picasso.get().load(url).placeholder(placeHolder).into(imageView);
        }
    }
}
