package net.stibbards.qcs.utils;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import net.stibbards.qcs.data.Commit;

import java.util.List;

public class CommitDiffCallback extends DiffUtil.Callback {

    private List<Commit> oldCommits;
    private List<Commit> newCommits;

    public CommitDiffCallback(List<Commit> oldCommits, List<Commit> newCommits) {
        this.oldCommits = oldCommits;
        this.newCommits = newCommits;
    }

    @Override
    public int getOldListSize() {
        return oldCommits.size();
    }

    @Override
    public int getNewListSize() {
        return newCommits.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldCommits.get(oldItemPosition).getSha().equals(newCommits.get(newItemPosition).getSha());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldCommits.get(oldItemPosition).equals(newCommits.get(newItemPosition));
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        //you can return particular field for changed item.
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }

}
