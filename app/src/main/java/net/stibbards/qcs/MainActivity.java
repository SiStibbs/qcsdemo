package net.stibbards.qcs;

import android.app.SearchManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import net.stibbards.qcs.adapters.CommitAdapter;
import net.stibbards.qcs.data.CommitsViewModel;
import net.stibbards.qcs.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private CommitAdapter adapter;
    private ActivityMainBinding binding;
    private CommitsViewModel commitsModel;

    private MenuItem searchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setIsLoading(true);

        adapter = new CommitAdapter();
        binding.list.setAdapter(adapter);

        commitsModel = ViewModelProviders.of(this).get(CommitsViewModel.class);
        commitsModel.getCommits().observe(this, commits -> {

        });

        getCommits(null);
        binding.refreshView.setOnRefreshListener(() -> {
            commitsModel.refreshData();
            binding.setIsLoading(true);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        searchItem = menu.findItem(R.id.action_search);

        setupSearch();
        return super.onCreateOptionsMenu(menu);
    }

    private void setupSearch() {
        if(searchItem != null) {
            SearchView searchView = (SearchView) searchItem.getActionView();

            SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
            if(searchManager != null)
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    getCommits(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    getCommits(newText);
                    return false;
                }
            });

            searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    getCommits(null);
                    return true;
                }
            });
        }
    }

    private void getCommits(String filterText) {
        String queryText = "%" + filterText + "%";
        commitsModel.searchQuery(TextUtils.isEmpty(filterText) ? null : queryText).observe(this, commits -> {
            if (commits != null) {
                binding.setIsLoading(false);
                adapter.setItems(commits);
            } else {
                binding.setIsLoading(true);
            }
            binding.refreshView.setRefreshing(false);
            binding.list.scrollToPosition(1);
            binding.executePendingBindings();
        });
    }
}
